const User = require('../models/user');
const Role = require('../models/roles');
const helpers = require('../util/helpers');
const io = require('../../socket');
const { USER_ROLES_ENUM } = require('../util/constants')

exports.getUser = async (req, res, next) => {
    const userId = req.params.id;
    User.findByPk(userId, {include: Role})
        .then(user => {
            if (!user) {
                const err = new Error("Couldn't find user!");
                err.statusCode = 404;
                throw err
            }
            let secureData = null

            if(user.role.role === USER_ROLES_ENUM.TUTOR){
                secureData = helpers.getSecureTutorData(user);
            } else {
                secureData = helpers.getSecureStudentData(user);
            }

            res.status(200).json({message: 'User data fetched successfully!', user: secureData});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })

};

exports.getProfile = async (req, res, next) => {
    const userId = req.userId;
    User.findByPk(userId, {include: Role})
        .then(user => {
            if (!user) {
                const err = new Error("Couldn't find user!");
                err.statusCode = 404;
                throw err
            }
            let secureData = null

            if(user.role.role === USER_ROLES_ENUM.TUTOR){
                secureData = helpers.getSecureTutorData(user);
            } else {
                secureData = helpers.getSecureStudentData(user);
            }

            res.status(200).json({message: 'User data fetched successfully!', user: secureData});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })

};

exports.getApplicants = async (req, res, next) => {
    const status = req.query.status;

    const role = await Role.findOne({ where: { role: USER_ROLES_ENUM.TUTOR } })
    User.findAll({where: { roleId: role.id, status: status }})
        .then(users => {

            res.status(200).json({message: 'Applicants fetched successfully!', docs: users});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })

};


exports.getSubjectTutors = (req, res, next) => {
    const subjectId = req.params.subjectId;
    User.findAll({where: {subjectId: subjectId}})
        .then(users => {

            res.status(200).json({message: 'Related tutors are fetched successfully!', docs: users});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
}

exports.getUserStatus = (req, res, next) => {
    User.findByPk(req.userId)
        .then(user => {
            if (!user) {
                const error = new Error('User not found.');
                error.statusCode = 404;
                throw error;
            }
            res.status(200).json({status: user.status});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};

exports.updateUserStatus = (req, res, next) => {
    const userId = req.params.id;
    const newStatus = req.body.status
    let secureData;
    User.findByPk(userId)
        .then(user => {
            if (!user) {
                const error = new Error('User not found.');
                error.statusCode = 404;
                throw error;
            }
            user.status = newStatus;
            return user.save();
        })
        .then(result => {
            if(result.role === USER_ROLES_ENUM.STUDENT){
                secureData = helpers.getSecureStudentData(result);
            }
            if(result.role === USER_ROLES_ENUM.TUTOR){
                secureData = helpers.getSecureTutorData(result);
            }
            // if(result.status === 'pending'){
            //     io.getIO().emit('applicants/pending', {action: 'update', user: secureData});
            // }if(result.status === 'confirmed'){
            //     io.getIO().emit('applicants/confirmed/'+result.id.toString(), {action: 'update', user: secureData});
            // }
            res.status(200).json({ message: 'User status updated.', doc: secureData });
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};
