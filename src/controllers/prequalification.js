const User = require('../models/user');
const Schedule = require('../models/schedule');

exports.saveProfile = async (req, res, next) => {
    const {
        birthDate, shortDescription,
        educationName, educationFaculty, educationDegree,
        resume, avatar
    } = req.body;

    const userId = req.userId
    try {
        const user = await User.findByPk(userId)
        if (!user) {
            const err = new Error("Couldn't find user!");
            err.statusCode = 404;
            throw err
        }
        user.birthDate = birthDate;
        user.shortDescription = shortDescription;
        user.educationName = educationName;
        user.educationFaculty = educationFaculty;
        user.educationDegree = educationDegree;
        user.resume = resume;
        user.avatar = avatar;
        user.isProfileCompleted = true
        await user.save()

        res.status(200).json({message: 'Profile saved successfully!'});
    }
    catch(err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }

};

exports.upploadFile = (req, res, next) => {
    if (!req.file) {
        const error = new Error("No file was found!!");
        error.statusCode = 422;
        throw error;
    }
    let fileUrl = req.file.path;
    res.status(200).json({message: 'File saved successfully!', fileUrl});
};


exports.saveCourse = (req, res, next) => {
    let {subjectId, price} = req.body;
    User.findByPk(req.userId)
        .then(user => {
            if (!user) {
                const err = new Error("Couldn't find user!");
                err.statusCode = 404;
                throw err
            }
            user.subjectId = subjectId;
            user.price = price;
            user.isCourseOfferCompleted = true
            return user.save()
        })
        .then(result => {
            res.status(200).json({message: 'Course offer saved successfully!'});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
};

exports.saveSchedule = async (req, res, next) => {
    try {
        let event = [
            {
              day: 1,
              start: '08:00',
              end: '14:00'
            },
            {
                day: 2,
                start: '08:00',
                end: '14:00'
            },
            {
                day: 3,
                start: '08:00',
                end: '14:00'
            },
            {
                day: 4,
                start: '08:00',
                end: '14:00'
            },
            {
                day: 5,
                start: '08:00',
                end: '14:00'
            },
            {
                day: 6,
                start: '08:00',
                end: '14:00'
            },
            {
                day: 7,
                start: '08:00',
                end: '14:00'
            },
        ]
        const { events } = req.body;
        const user = await User.findByPk(req.userId)

        if (!user) {
            const err = new Error("Couldn't find user!");
            err.statusCode = 404;
            throw err
        }


        if(!user.scheduleId){
            const schedule = await Schedule.create({
                events
            })

            user.scheduleId = schedule.id
            user.isScheduleCompleted = true
            await user.save()

            res.status(200).json({ message: 'User schedule saved successfully!', doc: schedule })
        }

        const existingSchedule = await Schedule.finByPk(user.scheduleId)
        if (!existingSchedule) {
            const err = new Error("Couldn't find current user's schedule!");
            err.statusCode = 404;
            throw err
        }

        existingSchedule.events = events
        await existingSchedule.save()

        res.status(200).json({ message: 'User schedule saved successfully!', doc: existingSchedule })
    }
    catch(err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};



