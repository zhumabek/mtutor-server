const helpers = require("../util/helpers");

const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const Roles = require('../models/roles');
const { USER_STATUSES, USER_ROLES_ENUM } = require('../util/constants');

exports.signup = async (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Validation failed.');
        error.statusCode = 422;
        error.data = errors.array();
        throw error;
    }
    let { email, password, phone, fullName, role: roleCode } = req.body;
    let userStatus = USER_STATUSES.WAITING;

    try {
        const currentRole = await Roles.findOne({ where: { role: roleCode }})

        if (!currentRole) {
            const error = new Error('A role with this code could not be found.');
            error.statusCode = 401;
            throw error;
        }
        if (currentRole.role !== USER_ROLES_ENUM.TUTOR) {
            userStatus = USER_STATUSES.CONFIRMED;
        }

        const hashedPw = await bcrypt.hash(password, 12)

        const user = new User({
            email,
            password: hashedPw,
            roleId: currentRole.id,
            phone,
            fullName,
            status: userStatus
        });
        await user.save();

        const token = jwt.sign(
            {
                email: user.email,
                userId: user.id
            },
            'somesupersecretsecret',
            {expiresIn: '1h'}
        );
        let secureData = { ...user.dataValues, role: await user.getRole() };

        if(currentRole.role === USER_ROLES_ENUM.TUTOR){
            secureData = helpers.getSecureTutorData(secureData);
        } else {
            secureData = helpers.getSecureStudentData(secureData);
        }

        res.status(200).json({ message: 'User successfully signed up!', token: token, user: secureData });

    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.login = (req, res, next) => {
    const email = req.body.email;
    const password = req.body.password;
    let loadedUser;
    User.findOne({where: { email }, include: Roles })
        .then(user => {
            if (!user) {
                const error = new Error('A user with this email could not be found.');
                error.statusCode = 401;
                throw error;
            }
            loadedUser = user;
            return bcrypt.compare(password, user.password);
        })
        .then(isEqual => {
            if (!isEqual) {
                const error = new Error('Wrong password!');
                error.statusCode = 401;
                throw error;
            }
            const token = jwt.sign(
                {
                    email: loadedUser.email,
                    userId: loadedUser.id
                },
                'somesupersecretsecret',
                {expiresIn: '1h'}
            );
            let secureData = null;

            if(loadedUser.role === USER_ROLES_ENUM.TUTOR){
                secureData = helpers.getSecureTutorData(loadedUser);
            } else {
                secureData = helpers.getSecureStudentData(loadedUser);
            }
            res.status(200).json({message: 'User successfully signed in!', token: token, user: secureData});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};
