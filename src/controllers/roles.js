const {validationResult} = require('express-validator/check');
const Roles = require('../models/roles');

exports.saveRole = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Validation failed, entered data is incorrect.');
        error.statusCode = 422;
        throw error;
    }

    let {role, name, data, id} = req.body;
    if (!id) {
        Roles.create({
            role,
            name,
            data
        })
            .then(result => {
                res.status(201).json({message: 'Role created successfully!', doc: result});
            })
            .catch(err => {
                if (!err.statusCode) {
                    err.statusCode = 500;
                }
                next(err);
            });
    } else {
        Roles.findByPk(id)
            .then(res => {
                if (!res) {
                    const error = new Error('Could not find role.');
                    error.statusCode = 404;
                    throw error;
                }
                res.role = role;
                res.name = name;
                res.data = data;
                return res.save();
            })
            .then(result => {
                res.status(200).json({message: 'Role updated successfully!', doc: result});
            })
            .catch(err => {
                if (!err.statusCode) {
                    err.statusCode = 500;
                }
                next(err);
            });
    }
};

exports.getRoles = async (req, res, next) => {
    try {
        const roles = await Roles.findAll();
        res.status(200).json({
            message: "Fetched roles successfully.",
            docs: roles
        });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.getRole = (req, res, next) => {
    const roleId = req.params.roleId;
    Roles.findByPk(roleId)
        .then(role => {
            if (!role) {
                const error = new Error('Could not find role.');
                error.statusCode = 404;
                throw error;
            }
            res.status(200).json({message: 'Role fetched.', doc: role});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};


exports.updateRole = (req, res, next) => {
    const roleId = req.params.roleId;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const error = new Error('Validation failed, entered data is incorrect.');
        error.statusCode = 422;
        throw error;
    }
    let {role, name, data} = req.body;
    Roles.findByPk(roleId)
        .then(res => {
            if (!res) {
                const error = new Error('Could not find role.');
                error.statusCode = 404;
                throw error;
            }
            res.role = role;
            res.name = name;
            res.data = data;
            return res.save();
        })
        .then(result => {
            res.status(200).json({message: 'Role updated successfully!', doc: result});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};

exports.deleteRole = (req, res, next) => {
    const roleId = req.params.roleId;
    Roles.findByPk(roleId)
        .then(role => {
            if (!role) {
                const error = new Error('Could not find role.');
                error.statusCode = 404;
                throw error;
            }

            return role.destroy();
        })
        .then(result => {
            res.status(200).json({message: 'Deleted role successfully.'});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};