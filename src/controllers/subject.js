const {validationResult} = require('express-validator');
const Subject = require('../models/subject');
const Sequelize = require('sequelize');

exports.getSubjects = async (req, res, next) => {
    const searchedSubjectName = req.query.name
    try {
        let whereParams = {}
        if(searchedSubjectName){
            whereParams = {
                where: {
                    name: { [Sequelize.Op.iLike]: `%${searchedSubjectName}%` },
                },
                offset: 0,
                limit: 8,
            }
        }
        const subjects = await Subject.findAll(whereParams);
        res.status(200).json({
            message: "Fetched subjects successfully.",
            docs: subjects
        });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
};

exports.getSubject = (req, res, next) => {
    const subject_id = req.params.id;
    Subject.findByPk(subject_id)
        .then(subject => {
            if (!subject) {
                const err = new Error("Couldn't find subject!");
                err.statusCode = 404;
                throw err
            }
            res.status(200).json({message: 'Subject data fetched successfully!', doc: subject});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })

};

exports.createSubject = (req, res, next) => {
    let {name, data} = req.body;
    Subject.create({name, data})
        .then(r => {
            res.status(200).json({message: 'Subject created successfully!', doc: r});
        })
        .catch(error => {
            if (!error.statusCode) {
                error.statusCode = 500;
            }
            next(error);
        })
};

exports.updateSubject = (req, res, next) => {
    let {name, data} = req.body;
    Subject.findByPk(req.params.id)
        .then(subject => {
            if (!subject) {
                const err = new Error("Couldn't find subject!!!");
                err.statusCode = 404;
                throw err;
            }
            subject.name = name;
            subject.data = data;
            subject.save();
        })
        .then(r => {
            res.status(200).json({message: "Subject updated successfully!!!", doc: r})
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
};

exports.deleteSubject = (req, res, next) => {
    Subject.findByPk(req.params.id)
        .then(subject => {
            if (!subject) {
                const error = new Error('Could not find subject!');
                error.statusCode = 404;
                throw error;
            }

            return subject.destroy();
        })
        .then(result => {
            res.status(200).json({message: 'Deleted subject successfully.'});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        });
};