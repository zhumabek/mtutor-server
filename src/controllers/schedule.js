const {validationResult} = require('express-validator');
const User = require('../models/user');
const Schedule = require('../models/schedule');

exports.putSchedule = (req, res, next) => {
    let {scheduleId, events} = req.body;
    if (!scheduleId) {
        Schedule.create({
            events
        }).then(r => {
            res.status(200).json({message: 'Schedule saved successfully!', doc: r})
        }).catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
    } else {
        Schedule.findByPk(scheduleId)
            .then(schedule => {
                if (!schedule) {
                    const err = new Error("Couldn't find schedule!");
                    err.statusCode = 404;
                    throw err
                }
                schedule.events = events;
                return schedule.save()
            })
            .then(result => {
                res.status(200).json({message: 'Schedule saved successfully!', doc: result});
            })
            .catch(err => {
                if (!err.statusCode) {
                    err.statusCode = 500;
                }
                next(err);
            })
    }

};

exports.getSchedule = (req, res, next) => {
    const scheduleId = req.params.id;
    Schedule.findByPk(scheduleId)
        .then(schedule => {
            if (!schedule) {
                const err = new Error("Couldn't find schedule!");
                err.statusCode = 404;
                throw err
            }
            res.status(200).json({message: 'Schedule fetched successfully!', doc: schedule});
        })
        .catch(err => {
            if (!err.statusCode) {
                err.statusCode = 500;
            }
            next(err);
        })
};

// exports.deleteEvent = (req, res, next) => {
//     let {eventId} = req.body;;
//     Schedule.findByPk(eventId)
//         .then(schedule => {
//             if (!schedule) {
//                 const err = new Error("Couldn't find schedule!");
//                 err.statusCode = 404;
//                 throw err
//             }
//             schedule.events.forEach((e, index) => {
//                 if(e.id === eventId){
//                     schedule
//                 }
//             })
//         })
//         .then(r => res.status(200).json({message: 'Event deleted successfully!'}))
//         .catch(err => {
//             if (!err.statusCode) {
//                 err.statusCode = 500;
//             }
//             next(err);
//         })
// };

