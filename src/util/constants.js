const USER_STATUSES = {
    CONFIRMED: 'confirmed',
    WAITING: 'waiting',
    PENDING: 'pending',
    REJECTED: 'rejected',
    IAM_NEW: 'I am new!',
}

const USER_ROLES_ENUM = {
    ADMIN: 1,
    TUTOR: 5,
    STUDENT: 10
}


module.exports = {
    USER_STATUSES,
    USER_ROLES_ENUM
}