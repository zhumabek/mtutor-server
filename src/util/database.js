const Sequelize = require('sequelize');

const sequelize = new Sequelize('node-app', 'postgres', 'postgres', {
    dialect: 'postgres',
    host: 'database',
    logging: false
});

module.exports = sequelize;
