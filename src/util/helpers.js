exports.getSecureStudentData = (user) => {
    return {
        id: user.id,
        email: user.email,
        username: user.fullName,
        phone: user.phone,
        status: user.status,
        role: user.role,
        data: user.data
    };
};

exports.getSecureTutorData = (user) => {
    delete user.password
    return user;
};