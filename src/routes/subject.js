const express = require('express');
const subjectCtrl = require('../controllers/subject');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.get('/get/:id', isAuth, subjectCtrl.getSubject);
router.put('/update/:id', isAuth, subjectCtrl.updateSubject);
router.post('/add', isAuth, subjectCtrl.createSubject);
router.delete('/delete/:id', isAuth, subjectCtrl.deleteSubject);
router.get('/list', subjectCtrl.getSubjects);

module.exports = router;
