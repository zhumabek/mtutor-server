const express = require('express');
const { body } = require('express-validator');
const prequalificationCtrl = require('../controllers/prequalification');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.post(
  '/profile/save', isAuth, prequalificationCtrl.saveProfile
);
router.post(
  '/course/save', isAuth, prequalificationCtrl.saveCourse
);
router.post(
  '/schedule/save', isAuth, prequalificationCtrl.saveSchedule
);
router.post(
  '/upload/resume', isAuth, prequalificationCtrl.upploadFile
);

router.post(
  '/upload/avatar', isAuth, prequalificationCtrl.upploadFile
);


module.exports = router;
