const express = require('express');
const { body } = require('express-validator/check');

const rolesController = require('../controllers/roles');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// router.post('/put', isAuth, rolesController.saveRole);
// router.put('/update/:roleId', isAuth, rolesController.updateRole);
// router.get('/list', isAuth, rolesController.getRoles);
// router.get('/get/:roleId', isAuth, rolesController.getRole);
// router.delete('/remove/:roleId', isAuth, rolesController.deleteRole);

router.post('/add', rolesController.saveRole);
router.put('/update/:roleId', rolesController.updateRole);
router.get('/list', rolesController.getRoles);
router.get('/get/:roleId', rolesController.getRole);
router.delete('/remove/:roleId', rolesController.deleteRole);

module.exports = router;