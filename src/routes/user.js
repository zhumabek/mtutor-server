const express = require('express');
const { body } = require('express-validator');
const userCtrl = require('../controllers/user');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.get(
    '/applicants', isAuth, userCtrl.getApplicants
);

router.get(
  '/profile/me',
    isAuth,
    userCtrl.getProfile
);

router.get(
  '/tutors/:subjectId', isAuth, userCtrl.getSubjectTutors
);

router.put(
  '/:id/status', isAuth, userCtrl.updateUserStatus
);

router.get(
    '/get/:id',
    isAuth,
    userCtrl.getUser
);

module.exports = router;
