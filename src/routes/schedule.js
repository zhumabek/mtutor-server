const express = require('express');
const { body } = require('express-validator');
const scheduleCtrl = require('../controllers/schedule');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.post(
  '/save', isAuth, scheduleCtrl.putSchedule
);
router.get(
  '/get/:id', isAuth, scheduleCtrl.getSchedule
);


module.exports = router;
