const Sequelize = require('sequelize');
const sequelize = require('../util/database');
const { USER_STATUSES } = require('../util/constants');

const User = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    fullName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    phone: {
        type: Sequelize.STRING,
        allowNull: false
    },
    status: {
        type: Sequelize.STRING,
        defaultValue: USER_STATUSES.IAM_NEW
    },
    roleId: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    birthDate: {
        type: Sequelize.STRING,
        allowNull: true
    },
    shortDescription: {
        type: Sequelize.STRING,
        allowNull: true
    },
    resume: {
        type: Sequelize.STRING,
        allowNull: true
    },
    avatar: {
        type: Sequelize.STRING,
        allowNull: true
    },
    educationDegree: {
        type: Sequelize.STRING,
        allowNull: true
    },
    educationFaculty: {
        type: Sequelize.STRING,
        allowNull: true
    },
    educationName: {
        type: Sequelize.STRING,
        allowNull: true
    },
    price: {
        type: Sequelize.FLOAT,
        defaultValue: 0.00
    },
    isProfileCompleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    isScheduleCompleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    isCourseOfferCompleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    data: {
        type: Sequelize.JSONB,
        allowNull: true
    }
});

module.exports = User;
