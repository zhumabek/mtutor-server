const Sequelize = require('sequelize');
const sequelize = require('../util/database');

const Schedule = sequelize.define('schedule', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    events: {
        type: Sequelize.JSONB,
        allowNull: false
    },
    data: {
        type: Sequelize.JSONB,
        allowNull: true
    }
});

module.exports = Schedule;