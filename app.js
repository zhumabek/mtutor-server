const path = require('path');
const fs = require('fs')

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const multer = require('multer');
const sequelize = require('./src/util/database');

const authRoutes = require('./src/routes/auth');
const rolesRoutes = require('./src/routes/roles');
const prequalificationRoutes = require('./src/routes/prequalification');
const userRoutes = require('./src/routes/user');
const subjectRoutes = require('./src/routes/subject');
const scheduleRoutes = require('./src/routes/schedule');

const User = require('./src/models/user');
const Role = require('./src/models/roles');
const Subject = require('./src/models/subject');
const Schedule = require('./src/models/schedule');

const app = express();

app.use(cors({
    credentials: true,
    origin: '*'
}))

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        const path = 'files'
        if (!fs.existsSync(path)){
            fs.mkdirSync(path, { recursive: true })
        }

        return cb(null, path)
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString().replace(/:/g, '-') + '-' + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg' ||
        file.mimetype === 'application/pdf' ||
        file.mimetype === 'application/docs'
    ) {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

// app.use(bodyParser.urlencoded()); // x-www-form-urlencoded <form>
app.use(bodyParser.json({ limit: '50mb' })); // application/json
app.use(
    multer({storage: fileStorage, fileFilter: fileFilter}).single('file')
);
app.use('/files', express.static(path.join(__dirname, '/files')));

// app.use((req, res, next) => {
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader(
//         'Access-Control-Allow-Methods',
//         'OPTIONS, GET, POST, PUT, PATCH, DELETE'
//     );
//     res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Content-Length, Authorization, Accept,X-Requested-With');
//     next();
// });

app.use('/auth', authRoutes);
app.use('/role', rolesRoutes);
app.use('/prequalification', prequalificationRoutes);
app.use('/user', userRoutes);
app.use('/subject', subjectRoutes);
app.use('/schedule', scheduleRoutes);

User.belongsTo(Subject, {constraints: true, onDelete: 'CASCADE'});
Subject.hasMany(User);
User.belongsTo(Role, {constraints: true, onDelete: 'CASCADE'});
Role.hasMany(User);
User.belongsTo(Schedule, {constraints: true, onDelete: 'CASCADE'});
Schedule.hasOne(User);

app.use((req, res, next) => {
   if(!req.userId){
       next()
   }
   User.findByPk(req.userId).then(user => req.user = user);
   next()
});

app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({message: message, data: data});
});

sequelize.sync({ alter: true})
    .then(r => {
        const server = app.listen(8000);
        const io = require('./socket').init(server);
        io.on('connection', socket => {
            console.log("Client conected");
        })
        console.log('Server started on port: ' + 8000)
    })
    .catch(err => {
        console.log(err)
    });

