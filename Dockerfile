FROM node:12.22.3-alpine3.14
RUN apk add --no-cache tzdata
ENV TZ Asia/Bishkek
RUN mkdir -p /app
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app

RUN npm install

COPY . .

EXPOSE 8000

CMD ["npm", "run", "start:prod"]